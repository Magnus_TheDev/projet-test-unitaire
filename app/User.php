<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    
    
    public function todolist()
    {
        return $this->belongsto(todolist::class, 'todolistid', 'id');
        
    }
    
    public function getAge(){
        return $this->age;
    }
    
    public static function IsValid(User $user){
        $isok = true;
    
        if (strlen($user->password) < 8 ) {
            $isok = false;
        }
    
        if (strlen($user->password) > 40 ) {
            $isok = false;
        }
    
        if (!filter_var($user->email, FILTER_VALIDATE_EMAIL)) {
            $isok = false;
        }
    
        if (!isset($user->name) && !isset($user->lastname)){
            $isok = false;
        }
        if ($user->age < 13){
            $isok = false;
        }
        
        return $isok;
    }
    
    
}
