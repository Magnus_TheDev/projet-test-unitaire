<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class itemlist extends Model
{
    protected $table = 'itemlist';
    
    public static function canAddItem(itemlist $itemlist){
        $itemlist->save();
        if($itemlist->wasRecentlyCreated){
            return $itemlist;
        }else{
            return null;
        }
    }
}
