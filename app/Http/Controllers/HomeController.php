<?php

namespace App\Http\Controllers;

use App\itemlist;
use App\todolist;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function index()
    {
        $user = Auth::user();
        
        
            $todo = new todolist();
    
            $todo->name = "test";
            $todo->save();
    
            $user->todolistid = $todo->id;
            $user->save();
    
            $item = new itemlist();
            $item->name = "test";
            $item->content = "test";
            $item->todoid = $todo->id;
            $item->save();
        
        dd($item);
        
        //var_dump($user->todolist->getlist());
        
        return view('home');
    }
}
