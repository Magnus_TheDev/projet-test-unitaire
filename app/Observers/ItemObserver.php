<?php

namespace App\Observers;

use App\itemlist;

class ItemObserver
{
    /**
     * Handle the itemlist "created" event.
     *
     * @param  \App\itemlist  $itemlist
     * @return void
     */
    public function created(itemlist $itemlist)
    {
        return false;
    }

    /**
     * Handle the itemlist "updated" event.
     *
     * @param  \App\itemlist  $itemlist
     * @return void
     */
    public function updated(itemlist $itemlist)
    {
        //
    }

    /**
     * Handle the itemlist "deleted" event.
     *
     * @param  \App\itemlist  $itemlist
     * @return void
     */
    public function deleted(itemlist $itemlist)
    {
        //
    }

    /**
     * Handle the itemlist "restored" event.
     *
     * @param  \App\itemlist  $itemlist
     * @return void
     */
    public function restored(itemlist $itemlist)
    {
        //
    }

    /**
     * Handle the itemlist "force deleted" event.
     *
     * @param  \App\itemlist  $itemlist
     * @return void
     */
    public function forceDeleted(itemlist $itemlist)
    {
        //
    }
}
