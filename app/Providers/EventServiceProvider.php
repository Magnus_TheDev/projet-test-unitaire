<?php
    
    namespace App\Providers;
    
    use App\itemlist;
    use App\MailerSend;
    use App\User;
    use Carbon\Carbon;
    use Illuminate\Auth\Events\Registered;
    use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
    use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
    use Illuminate\Support\Facades\Hash;

    class EventServiceProvider extends ServiceProvider
    {
        /**
         * The event listener mappings for the application.
         *
         * @var array
         */
        protected $listen = [
            Registered::class => [
                SendEmailVerificationNotification::class,
                itemlist::class,
            ],
        ];
        
        /**
         * Register any events for your application.
         *
         * @return void
         */
        public function boot()
        {
            parent::boot();
            User::creating(function($user){
                $isok = true;
    
                if (strlen($user->password) < 8 ) {
                    $isok = false;
                }
    
                if (strlen($user->password) > 40 ) {
                    $isok = false;
                }
                
                if (!filter_var($user->email, FILTER_VALIDATE_EMAIL)) {
                    $isok = false;
                }
                
                if (!isset($user->name) && !isset($user->lastname)){
                    $isok = false;
                }
    
                if ($user->age < 13){
                    $isok = false;
                }
                if($isok === false){
                    return $isok;
                }
                if($isok === true){
                   MailerSend::sendMail($user);
                }
                $user->password = Hash::make($user->password);
            });
            
            itemlist::creating(
                function ($item) {
                    $isok = true;
                    
                    if (strlen($item->content) >= 1000) {
                        $isok = false;
                    }
    
                    if (!isset($item->name)) {
                        $isok = false;
                    }
                    
                    if (itemlist::where('name', $item->name)->count() !== 0) {
                        $isok = false;
                    }
                    if (itemlist::where('todoid', $item->todoid)->count() >= 10) {
                        $isok = false;
                    }
                    
                    $lastItem = itemlist::where('todoid', $item->todoid)->latest('created_at')->first();
                    
                    if ($lastItem != []) {
                        $start = Carbon::now();
                        $end = new Carbon($lastItem->created_at);
                        
                        if ($end->diffInMinutes($start) < 30) {
                            return false;
                        }
                    }
                    
                    return $isok;
                }
            );
        }
    }
