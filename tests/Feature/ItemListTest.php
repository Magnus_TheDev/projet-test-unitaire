<?php

namespace Tests\Feature;

use App\itemlist;
use Faker\Factory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ItemListTest extends TestCase
{
    use RefreshDatabase;
    
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testTodoInsert(){
        $faker = Factory::create("fr_FR");
        
        $item = new itemlist();
        $item->name = $faker->name;
        $item->content = $faker->text(50);
        $item->todoid = 1;
        $this->assertInstanceOf('App\itemlist',itemlist::canAddItem($item));
    }
    
    public function testTodoNoName(){
        $faker = Factory::create("fr_FR");
        
        $item = new itemlist();
        $item->content = $faker->text(50);
        $item->todoid = 1;
        $this->assertFalse($item->save());
    }
    
    public function testTodoDualName(){
        $faker = Factory::create("fr_FR");
        
        $item1 = new itemlist();
        $item1->name = "test";
        $item1->content = $faker->text(50);
        $item1->todoid = 1;
        $item1->save();
    
        $item2 = new itemlist();
        $item2->name = "test";
        $item2->content = $faker->text(50);
        $item2->todoid = 1;
        
        $this->assertFalse($item2->save());
    }
    
    
    public function testContentMax_1000Char(){
        $faker = Factory::create("fr_FR");
        
        $item = new itemlist();
        $item->name = $faker->name;
        $item->content = $faker->text(500000);
        $item->todoid = 1;
        $this->assertFalse($item->save());
    }
    
    public function test_30MinutesDiff(){
        $faker = Factory::create("fr_FR");
        
        $item1 = new itemlist();
        $item1->name = $faker->name;
        $item1->content = $faker->text(50);
        $item1->todoid = 1;
        $item1->save();
        
        $item2 = new itemlist();
        $item2->name = $faker->name;
        $item2->content = $faker->text(50);
        $item2->todoid = 1;
        
        $this->assertFalse($item2->save());
    }
    
    public function testCanAddItemFalse(){
        $faker = Factory::create("fr_FR");
        
        $item = new itemlist();
        $item->name = $faker->name;
        $item->content = $faker->text(10001);
        $item->todoid = 1;
        $this->assertNull(itemlist::canAddItem($item));
    }
    
}
