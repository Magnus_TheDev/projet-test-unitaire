<?php
    
    namespace Tests\Feature;
    
    use App\MailerSend;
    use App\User;
    use Faker\Factory;
    use Illuminate\Foundation\Testing\RefreshDatabase;
    use Tests\TestCase;
    
    
    class UserTest extends TestCase
    {
        use RefreshDatabase;
        /**
         * A basic feature test example.
         *
         * @return void
         */
        public function testUserInsert()
        {
            $faker = Factory::create("fr_FR");
            $email = $faker->email;
            
            $user = New User();
            $user->name = $faker->name;
            $user->lastName = $faker->lastName;
            $user->email = $email;
            $user->password = $faker->password(23,35);
            $user->age = rand('12','54');
            $user->save();
            
            
            $this->assertDatabaseHas('users', [
                'email' => $email,
            ]);
        }
    
    
        public function testUserFailEmail()
        {
            $faker = Factory::create("fr_FR");
            $email = "testemailfail";
        
            $user = New User();
            $user->name = $faker->name;
            $user->lastName = $faker->lastName;
            $user->email = $email;
            $user->password = $faker->password;
            $user->age = rand('12','54');
            
            $this->assertFalse($user->save());
        }
    
        public function testUserNoInput()
        {
            $faker = Factory::create("fr_FR");
            $email = $faker->email;
        
            $user = New User();
            $user->email = $email;
            $user->password = $faker->password;
            $user->age = rand('12','54');
        
            $this->assertFalse($user->save());
        }
    
        public function testUserPasswordFail_8_Char()
        {
            $faker = Factory::create("fr_FR");
            $email = $faker->email;
    
            $user = New User();
            $user->name = $faker->name;
            $user->lastName = $faker->lastName;
            $user->email = $email;
            $user->password = '****';
            $user->age = rand('12','54');
        
            $this->assertFalse($user->save());
        }
    
        public function testUserPasswordFail_42_Char()
        {
            $faker = Factory::create("fr_FR");
            $email = $faker->email;
        
            $user = New User();
            $user->name = $faker->name;
            $user->lastName = $faker->lastName;
            $user->email = $email;
            $user->password = '************************************************';
            $user->age = rand('12','54');
        
            $this->assertFalse($user->save());
        }
    
    
        public function testUserhasNot_13()
        {
            $faker = Factory::create("fr_FR");
            $email = $faker->email;
        
            $user = New User();
            $user->name = $faker->name;
            $user->lastName = $faker->lastName;
            $user->email = $email;
            $user->password = $faker->password;
            $user->age = 12;
        
            $this->assertFalse($user->save());
        }
    
        public function testSendMail()
        {
            $user = $this->getMockBuilder('App\User')
                ->setMethods(['getAge'])->getMock();
            $user->method("getAge")->will($this->returnValue(18));
    
            $this->assertTrue(MailerSend::sendMail($user));
    
        }
    
        public function testSendMailFail()
        {
            $user = $this->getMockBuilder('App\User')
                ->setMethods(['getAge'])->getMock();
            $user->method("getAge")->will($this->returnValue(3));
        
            $this->assertFalse(MailerSend::sendMail($user));
        }
        
        public function testIsValid(){
            $faker = Factory::create("fr_FR");
            $email = $faker->email;
    
            $user = New User();
            $user->name = $faker->name;
            $user->lastName = $faker->lastName;
            $user->email = $email;
            $user->password = $faker->password(23,35);
            $user->age = 19;
    
            $this->assertTrue(User::IsValid($user));
        }
    
        public function testIsValidFalse(){
            $faker = Factory::create("fr_FR");
            $email = $faker->email;
        
            $user = New User();
            $user->name = $faker->name;
            $user->lastName = $faker->lastName;
            $user->email = $email;
            $user->password = $faker->password(23,35);
            $user->age = 12;
        
            $this->assertFalse(User::IsValid($user));
        }
        
    }
